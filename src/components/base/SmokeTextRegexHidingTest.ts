const getElementByXpath = (path) =>
  document.evaluate(
    path, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null,
  )
    .singleNodeValue;

export default function(element: HTMLElement) {
  const failed = [];

  if (/18\+/.test(element.innerHTML)) {
    console.warn('item was found 1');
    failed.push(1);
  }

  // этот тест странный, и найдет м о с к в а, думается, в большинстве текстов
  if (/м.*о.*с.*к.*в.*а/i.test(element.innerHTML)) {
    console.warn('item was found 2');
    const m1 = element.innerHTML.match(/м.*о.*с.*к.*в.*а/i);
    console.log(
      '/м.*о.*с.*к.*в.*а/i найдено тут:',
      element.innerHTML.slice(m1.index, m1.index + 120),
    );
    failed.push(2);
  }

  if (getElementByXpath('//*[@id="root"]/div//div[contains(text(), "18+")]')) {
    console.warn('item was found 3');
    failed.push(3);
  }

  if (getElementByXpath('//*[@id="root"]/div//img')) {
    console.warn('item was found 4');
    failed.push(4);
  }

  if (/18\+/.test(element.innerText)) {
    console.warn('item was found 5');
    failed.push(5);
  }

  if (getElementByXpath('//*[@id="root"]/div//div[contains(@class, "img")]')) {
    console.warn('item was found 6');
    failed.push(6);
  }

  if (getElementByXpath('//*[@id="root"]/div//div[@style]')) {
    console.warn('item was found 7');
    failed.push(7);
  }

  return failed;
}
