import * as React from 'react';

import {random} from '../..//utils/utils';

// впрочем, можно усложнить и сделать псевдослучайный генератор
// https://stackoverflow.com/questions/521295/seeding-the-random-number-generator-in-javascript
// тогда можно будет получать имя класса по урлу (и размеру), и возможно, переводить это обратно
const genImgClassName = (width, height) =>
  'mimic_i_' + random(1, 10e5) + `_${width}` + `_${height}`;

export default (props: React.ImgHTMLAttributes<HTMLImageElement>) => {
  const {width, height, src} = props;
  const className = genImgClassName(width, height);
  const style = `
  div.${className} {
    width: ${width}px;
    height: ${height}px;
    background-image: url(${src});
    background-size: cover;
    background-position: center;
  }
  `;
  return (
    <>
      <style type='text/css'>
        {style}
      </style>
      <div className={className}/>
    </>
  );
};
