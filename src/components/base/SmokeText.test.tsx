import * as React from 'react';
import * as ReactDOM from 'react-dom/client';

import {cleanup} from '@testing-library/react';
import {act} from 'react-dom/test-utils';

import SmokeText from './SmokeText';
import runSmokeTextRegexHidingTest from './SmokeTextRegexHidingTest';


let mockDiv = null;
let root = null;
let spy = null;
const msg = 'Текст скроется на фазе 2';
const msgs = [
  '18+',
  '<span>18</span>+',
  '<span>1</span> <div>8</div> <p>+</p>',
  'Москва',
  '<span>Мос</span> <span>ква</span>',
];
const mockDivMsg = 'hello_it_is_mock';

beforeEach(() => {
  mockDiv = document.createElement('div');
  mockDiv.dataset.test = mockDivMsg;
  root = ReactDOM.createRoot(mockDiv);
  spy = jest.spyOn(mockDiv, 'attachShadow');
});

afterEach(() => {
  mockDiv = null;
  root = null;
  spy = null;
  cleanup();
});

describe('SmokeText compoment test', () => {
  // можно было бы использовать это
  // https://www.npmjs.com/package/testing-library__dom
  it('Span placeholder on phases', async () => {
    act(() => {
      root.render(
        <SmokeText>
          <p>{msg}</p>
        </SmokeText>,
      );
    });

    // проверка, что внутри ничего нет. Фаза 1.
    expect(mockDiv.outerHTML === `<div data-test="${mockDivMsg}"></div>`)
      .toBeTruthy();

    // проверка, что создается shadow dom с children. Фаза 2.
    await expect(spy).toHaveBeenCalledTimes(1);
    // firstChild содержит ссылки на пропсы и Fiber Node
    expect((mockDiv.shadowRoot as unknown as HTMLElement)
      .innerHTML === `<p>${msg}</p>`).toBeTruthy();
  });


  msgs.forEach((msg, idx) => it('regex hiding: ' + (idx + 1), async () => {
    act(() => {
      root.render(
        <SmokeText>
          {msg}
        </SmokeText>,
      );
    });

    // просто ждем, пока отрендерится
    await expect(spy).toHaveBeenCalledTimes(1);
    expect(
      runSmokeTextRegexHidingTest(mockDiv).length === 0,
    )
      .toBeTruthy();
  }));
});
