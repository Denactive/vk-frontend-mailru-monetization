import * as React from 'react';
import * as ReactDOM from 'react-dom';

/*  -- ИДЕЯ --
1. фиктивное монтирование с целью заполучить ссылку на родителя
2. создание у родителя shadow dom и перенос туда children через Portal
*/

interface SmokeTextPropsI {
 children: React.ReactNode,
 stylesheets?: StyleSheet,
}

/**
 * Оборачивает содержимое в #shadow-root (open)
 *
 * Стилизовать ShadowDOM можно, передав элемент style в качестве дочернего
 * или через new CSSStyleSheet()
 *
 * @param {React.ReactNode} props.children
 * @param {StyleSheet} [props.stylesheets]
 * @return {JSX}
 */
const SmokeText = ({children, stylesheets}: SmokeTextPropsI) => {
  const emptySpanRef = React.useRef<HTMLSpanElement>(null);
  const [shadow, setShadow] = React.useState<ShadowRoot>(null);

  React.useEffect(() => {
    const host = emptySpanRef.current.parentNode as HTMLElement;

    // защита от "оптимизаций" React: двойного рендера без вызова эффекта при старте
    if (host.shadowRoot === null) {
      host.attachShadow({mode: 'open'});
    }

    const shadow = host.shadowRoot;

    setShadow(shadow);

    // https://web.dev/constructable-stylesheets/#using-constructed-stylesheets
    // https://github.com/microsoft/TypeScript/issues/30022
    if (stylesheets) {
      // eslint-disable-next-line no-undef
      (shadow as unknown as DocumentOrShadowRoot &{adoptedStyleSheets: StyleSheet})
        .adoptedStyleSheets = stylesheets;
    }

    return () => {
      // создать shadowRoot на родительском элементе можно только единожды
      shadow.innerHTML = '';
    };
  }, [stylesheets]);

  return (
    shadow ?
      ReactDOM.createPortal(children, shadow as unknown as Element) :
      <span ref={emptySpanRef}></span>
  );
};

export default SmokeText;
