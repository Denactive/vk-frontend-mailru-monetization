/**
	* Метод возвращает случайное число из заданного диапазона
	*
	* @param {number} min — Минимальное значение
	* @param {number} max — Максимальное значение
	* @return {number}
	*/
export function random(min, max) {
  return (Math.random() * (max - min + 1) + min) | 0;
} 
