import React from 'react';
import ReactDOM from 'react-dom/client';
import RenderComponent from './stories/RenderComponent';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <RenderComponent />
  </React.StrictMode>,
);
