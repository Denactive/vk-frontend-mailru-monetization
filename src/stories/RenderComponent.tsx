import * as React from 'react';
import Locator from 'smokescreen/Locator';

import Container from '@/components/main/index';
import runSmokeTextRegexHidingTest
  from '@/components/base/SmokeTextRegexHidingTest';

export const locatorMatches = [
  'mimic',

  'wrapper',
  'contacts',
  'content',
  'picture',
  'title',
  'warning',

  'teaser',
];

const response = require(`../responses/teaser-2.json`);
const items = response.result.body.direct.ads;

const locator = new Locator({
  enable: true,
  match: locatorMatches,
});

const transform = locator.transform.bind(locator);

export default class RenderComponent extends React.Component<any> {
  node: React.RefObject<HTMLDivElement> = React.createRef();

  componentDidMount() {
    const element = this.node.current;
    runSmokeTextRegexHidingTest(element);
  }

  render() {
    return (
      <div ref={this.node}>
        <Container
          items={items}
          transform={transform}
          locator={locator}
        />
      </div>
    );
  }
}
