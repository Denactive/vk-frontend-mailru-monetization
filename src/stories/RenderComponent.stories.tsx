import * as React from 'react';
import {ComponentMeta} from '@storybook/react';

import RenderComponent from './RenderComponent';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Root/Banner',
  component: RenderComponent,
} as ComponentMeta<typeof RenderComponent>;

export const Default = () => <RenderComponent />;
